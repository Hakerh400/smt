#pragma once

#include "game.hh"

typedef std::function<
  Game*(const std::vector<int>&)
> GamesInfoFnType;
typedef std::map<std::string, GamesInfoFnType> GamesInfoMpType;

class GamesInfo {
public:
  static const GamesInfoMpType& get_mp();
  static bool has(std::string);
  static const GamesInfoFnType& get(std::string);
  static Game* mk(std::string, const std::vector<int>&);

private:
  static GamesInfoMpType init_mp();
  static GamesInfoMpType mp;
};