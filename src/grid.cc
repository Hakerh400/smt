#include "log.hh"
#include "grid.hh"
#include "csp.hh"

template <typename T>
void Grid<T>::mk(int w, int h, std::function<T(int, int)> fn){
  this->w = w;
  this->h = h;
  size = w * h;
  data = std::vector<T>(size);

  for(int y = 0; y != h; y++){
    for(int x = 0; x != w; x++){
      int i = w * y + x;
      data[i] = fn(x, y);
    }
  }
}

template <typename T>
Grid<T>::Grid(int w, int h, std::function<T(int, int)> fn){
  mk(w, h, fn);
}

template <typename T>
Grid<T>::Grid(int w, int h){
  mk(w, h, [&](int x, int y){ return T(); });
}

template <typename T>
int Grid<T>::get_width() const {
  return w;
}

template <typename T>
int Grid<T>::get_height() const {
  return h;
}

template <typename T>
int Grid<T>::get_size() const {
  return size;
}

template <typename T>
std::vector<T>& Grid<T>::get_data(){
  return data;
}

template <typename T>
bool Grid<T>::has(int x, int y) const {
  if(x < 0 || x >= w) return 0;
  if(y < 0 || y >= h) return 0;
  return 1;
}

template <typename T>
T& Grid<T>::get(int x, int y){
  assert(has(x, y));
  return data[w * y + x];
}

template <typename T>
T* Grid<T>::getm(int x, int y){
  if(!has(x, y)) return nullptr;
  return &data[w * y + x];
}

template <typename T>
void Grid<T>::set(int x, int y, const T& val){
  get(x, y) = val;
}

template <typename T>
std::vector<T> Grid<T>::to_vector() const {
  return data;
}

template <typename T>
void Grid<T>::iter(std::function<void(int, int, T&)> fn){
  for(int y = 0; y != h; y++){
    for(int x = 0; x != w; x++){
      fn(x, y, get(x, y));
    }
  }
}

template <typename T>
Grid<T> Grid<T>::map(std::function<T(int, int, T&)> fn){
  Grid<T> grid1 = *this;

  for(int y = 0; y != h; y++){
    for(int x = 0; x != w; x++){
      grid1.set(x, y, fn(x, y, get(x, y)));
    }
  }

  return grid1;
}

template class Grid<int>;
template class Grid<Bit>;
template class Grid<BitVec>;
template class Grid<Nat>;