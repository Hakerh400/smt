#pragma once

#include "log.hh"

class CSP;

class Bit {
public:
  Bit() = default;
  Bit(CSP*, int);
  CSP* get_csp() const;
  int get_lit() const;
  Bit operator!() const;
  Bit operator||(const Bit&) const;
  Bit operator&&(const Bit&) const;
  Bit operator==(const Bit&) const;
  Bit operator!=(const Bit&) const;
  Bit operator^(const Bit&) const;
  Bit imp(const Bit&) const;
  Bit ite(const Bit&, const Bit&) const;

private:
  CSP* csp;
  int lit;
};