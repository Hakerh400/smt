#include "csp.hh"
#include "bit.hh"

Bit::Bit(CSP* csp, int lit){
  this->csp = csp;
  this->lit = lit;
}

CSP* Bit::get_csp() const {
  return csp;
}

int Bit::get_lit() const {
  return lit;
}

Bit Bit::operator!() const {
  return Bit(csp, -lit);
}

Bit Bit::operator||(const Bit& bit_b) const {
  int a = lit;
  int b = bit_b.lit;
  
  if(a == -1) return bit_b;
  if(b == -1) return *this;
  if(a == 1 || b == 1) return csp->bit(1);
  
  const Bit var_c = csp->mk_bit();
  int c = var_c.lit;

  csp->add_lit(-a);
  csp->add_lit(c);
  csp->add_lit(0);

  csp->add_lit(-b);
  csp->add_lit(c);
  csp->add_lit(0);

  csp->add_lit(a);
  csp->add_lit(b);
  csp->add_lit(-c);
  csp->add_lit(0);

  return var_c;
}

Bit Bit::operator&&(const Bit& bit_b) const {
  int a = lit;
  int b = bit_b.lit;
  
  if(a == 1) return bit_b;
  if(b == 1) return *this;
  if(a == -1 || b == -1) return csp->bit(0);
  
  const Bit var_c = csp->mk_bit();
  int c = var_c.lit;

  csp->add_lit(a);
  csp->add_lit(-c);
  csp->add_lit(0);

  csp->add_lit(b);
  csp->add_lit(-c);
  csp->add_lit(0);

  csp->add_lit(-a);
  csp->add_lit(-b);
  csp->add_lit(c);
  csp->add_lit(0);

  return var_c;
}

Bit Bit::operator==(const Bit& bit_b) const {
  int a = lit;
  int b = bit_b.lit;
  
  if(a == 1) return bit_b;
  if(b == 1) return *this;
  if(a == -1) return !bit_b;
  if(b == -1) return !*this;
  
  const Bit var_c = csp->mk_bit();
  int c = var_c.lit;

  csp->add_lit(-a);
  csp->add_lit(b);
  csp->add_lit(-c);
  csp->add_lit(0);

  csp->add_lit(a);
  csp->add_lit(-b);
  csp->add_lit(-c);
  csp->add_lit(0);

  csp->add_lit(-a);
  csp->add_lit(-b);
  csp->add_lit(c);
  csp->add_lit(0);
  
  csp->add_lit(a);
  csp->add_lit(b);
  csp->add_lit(c);
  csp->add_lit(0);

  return var_c;
}

Bit Bit::operator!=(const Bit& bit_b) const {
  return !(*this == bit_b);
}

Bit Bit::operator^(const Bit& bit_b) const {
  return *this != bit_b;
}

Bit Bit::imp(const Bit& bit_b) const {
  return !*this || bit_b;
}

Bit Bit::ite(const Bit& b, const Bit& c) const {
  const Bit& a = *this;
  return a.imp(b) && (!a).imp(c);
}