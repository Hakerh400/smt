#pragma once

#include "../game.hh"

class Sudoku : public Game {
public:
  static std::string get_name();

  Sudoku(const std::vector<int>& params) :
    Game(params){}
  virtual std::vector<int> generate();
};