#pragma once

#include "../game.hh"

class Fillomino : public Game {
public:
  static std::string get_name();

  Fillomino(const std::vector<int>& params) :
    Game(params){}
  virtual std::vector<int> generate();
};