#include "../log.hh"
#include "../grid.hh"
#include "fillomino.hh"

std::string Fillomino::get_name(){
  return "fillomino";
}

std::vector<int> Fillomino::generate(){
  CSP* csp = get_csp();
  const std::vector<int>& params = get_params();

  int w = params.at(0);
  int h = params.at(1);
  int ub = params.at(2);
  int mx = ub;

  int len = w * h;

  // mat <- mk_mat size # \(y, x) -> do
  //   v <- csp_mk_var_lt ub1
  //   csp_assert # csp_ors v
  //   return v

  Grid<Nat> grid = Grid<Nat>(w, h, [&](int x, int y){
    Nat v = csp->mk_nat(mx);
    csp->add_clue(v);
    csp->asrt(v.nonzero());
    return v;
  });

  // hmat <- mk_mat (h - 1, w) # \p@(y, x) -> do
  //   let a = mat_get p mat
  //   let b = mat_get (y + 1, x) mat
  //   fr # csp_eq1 a b

  Grid<Bit> hgrid = Grid<Bit>(w, h - 1, [&](int x, int y){
    Nat a = grid.get(x, y);
    Nat b = grid.get(x, y + 1);
    return a == b;
  });
  
  // vmat <- mk_mat (h, w - 1) # \p@(y, x) -> do
  //   let a = mat_get p mat
  //   let b = mat_get (y, x + 1) mat
  //   fr # csp_eq1 a b

  Grid<Bit> vgrid = Grid<Bit>(w - 1, h, [&](int x, int y){
    Nat a = grid.get(x, y);
    Nat b = grid.get(x + 1, y);
    return a == b;
  });

  // mat_map mat # \p0 n -> do

  grid.iter([&](int x0, int y0, Nat n){
  
    // mat1 <- mk_mat size # \p -> pure # do
    //   let d = dist p p0
    //   True <- pure # d < ub
    //   Just # csp_prop # d == 0
  
    Grid<int> grid1(w, h, [&](int x, int y){
      int d = abs(x - x0) + abs(y - y0);
      return d < ub ? csp->lit(d == 0) : 0;
    });
  
    // mat1 <- iterM (ub - 1) mat1 # \mat1 ->

    for(int i = 0; i != ub - 1; i++){

      // if(x0 == 0 && y0 == 0 && i == 1){
      //   log("<");
      //   grid1.iter([&](int x, int y, int b){
      //     log(b ? b == 1 ? "1" : b == -1 ? "0" : "#" : ".", 0);
      //     if(x == w - 1) log();
      //     // log(b);
      //   });
      //   log(">");
      //   exit(0);
      // }

      // mat_map mat1 # \p@(y, x) b -> case b of

      grid1 = grid1.map([&](int x, int y, int b_lit){
      
        // Nothing -> pure Nothing
        // Just b -> do
      
        if(!b_lit) return 0;

        Bit b = Bit(csp, b_lit);

        // adjs <- pure # do
        //   (Just b1, Just (Just b2)) <-
        //     [ (mat_get' (y - 1, x) hmat, mat_get' (y - 1, x) mat1)
        //     , (mat_get' (y, x - 1) vmat, mat_get' (y, x - 1) mat1)
        //     , (mat_get' (y, x) vmat, mat_get' (y, x + 1) mat1)
        //     , (mat_get' (y, x) hmat, mat_get' (y + 1, x) mat1)
        //     ]
        //   return # csp_and b1 b2
        // b <- fr # csp_ors # b : adjs
        // return # Just b

        std::vector<std::pair<int, int>> coords {
          {x, y - 1}, {x, y - 1},
          {x - 1, y}, {x - 1, y},
          {x, y}, {x + 1, y},
          {x, y}, {x, y + 1},
        };

        for(int i = 0; i != 8; i += 2){
          const std::pair<int, int>& cs1 = coords.at(i);
          const std::pair<int, int>& cs2 = coords.at(i + 1);

          int x1 = cs1.first;
          int y1 = cs1.second;
          int x2 = cs2.first;
          int y2 = cs2.second;

          Grid<Bit>& g = i == 0 || i == 6 ? hgrid : vgrid;

          if(!g.has(x1, y1)) continue;
          if(!grid1.has(x2, y2)) continue;

          int b2_lit = grid1.get(x2, y2);
          if(!b2_lit) continue;

          Bit b1 = g.get(x1, y1);
          Bit b2 = Bit(csp, b2_lit);

          b = b || (b1 && b2);
        }

        return b.get_lit();
      });
    }

    // n1 <- csp_count s # catMaybes # mat_elems mat1
    // csp_assert # csp_eq n n1

    Nat n1 = csp->nat(mx, 0);

    grid1.iter([&](int x, int y, int b_lit){
      if(!b_lit) return;
      n1 = n1.inc(Bit(csp, b_lit));
    });

    csp->asrt(n1 == n);
  });
  
  // -----
  // mapM_ <~ [vmat, hmat] # \mat ->
  //   mat_map mat # \p (ExprLit i 1) -> do
  //     True <- trace' p csp_solve
  //     b0 <- csp_get_var i
  //     let b = 1
  //     if b0 == b
  //       then csp_assert # csp_lit i b
  //       else do
  //         csp_assume i b
  //         sat <- csp_solve
  //         if sat
  //           then csp_assert # csp_lit i b
  //           else csp_assert # csp_lit i # inv_bit b
  // -----
  
  // sat <- csp_solve
  // if not sat then lift # putStrLn "/" else do
  //   rows <- mapM (mapM csp_eval_n) # mat_to_list mat
  //   lift # putStrLn # unlines' # map <~ rows # \row ->
  //     concat # map show row

  bool sat = csp->generate();
  if(!sat) return {};

  csp->filter_clues();
  std::vector<int> out = std::vector<int>(len);

  for(int y = 0; y != h; y++){
    for(int x = 0; x != w; x++){
      int i = y * w + x;

      const Nat& v = grid.get(x, y);
      int n = csp->get_nat(v);

      if(!csp->has_clue(v))
        n = ~n;

      out.at(i) = n;
    }
  }

  return out;
}