#include "../log.hh"
#include "sudoku.hh"

std::string Sudoku::get_name(){
  return "sudoku";
}

std::vector<int> Sudoku::generate(){
  CSP* csp = get_csp();
  const std::vector<int>& params = get_params();

  int ws = params.at(0);
  int hs = params.at(1);

  if(ws <= 0 || hs <= 0)
    return {};

  int n = ws * hs;
  int n2 = n * n;
  int mx = n - 1;

  std::vector<Nat> grid = std::vector<Nat>(n2);

  for(int i = 0; i != n2; i++){
    Nat d = csp->mk_nat(mx);
    csp->add_clue(d);
    grid[i] = d;
  }

  for(int k = 0; k != n; k++){
    for(int i = 0; i != n; i++){
      for(int j = i + 1; j != n; j++){
        csp->asrt(grid[n * k + i] != grid[n * k + j]);
        csp->asrt(grid[n * i + k] != grid[n * j + k]);
      }
    }
  }

  for(int yy = 0; yy != n; yy += hs){
    for(int xx = 0; xx != n; xx += ws){
      for(int i = 0; i != n; i++){
        for(int j = i + 1; j != n; j++){
          int y1 = yy + i / ws;
          int x1 = xx + i % ws;
          int y2 = yy + j / ws;
          int x2 = xx + j % ws;

          csp->asrt(grid[n * y1 + x1] != grid[n * y2 + x2]);
        }
      }
    }
  }

  bool sat = csp->generate();
  if(!sat) return {};
  
  csp->filter_clues();
  std::vector<int> out = std::vector<int>(n2);

  for(int i = 0; i != n2; i++){
    const Nat& d = grid[i];
    int n = csp->get_nat(d);

    if(!csp->has_clue(d))
      n = ~n;

    out.at(i) = n;
  }

  return out;
}