#pragma once

#include <iostream>
#include <cassert>
#include <cmath>
#include <vector>
#include <set>
#include <map>
#include <map>
#include <functional>

#define log _log_aux
#define floor _floor_aux
#define ceil _ceil_aux
#define sqrt _sqrt_aux
#define min _min_aux
#define max _max_aux
#define hypot _hypot_aux
#define dist _dist_aux
#define pow _pow_aux
#define sin _sin_aux
#define cos _cos_aux

int floor(double);
int ceil(double);
double sqrt(double);
int bound(int, int, int);
double bound(double, double, double);
double min(double, double);
double max(double, double);
double hypot(double, double);
double dist(double, double, double, double);
double pow(double, double);
double sin(double);
double cos(double);
double log_2(double);

int read_int();
std::string read_str();
std::vector<int> read_int_vec();