#pragma once

#include "bit_vec.hh"

class Nat : public BitVec {
public:
  Nat() = default;
  Nat(CSP* csp, std::vector<int> lits) :
    BitVec(csp, lits){}
  Nat(const BitVec& x) : BitVec(x){}
  Bit is_zero() const;
  Bit nonzero() const;
  Bit le_or_lt(bool, const Nat&) const;
  Nat inc(const Bit& c) const;
  Nat inc() const;
  Bit operator<=(const Nat&) const;
  Bit operator>=(const Nat&) const;
  Bit operator<(const Nat&) const;
  Bit operator>(const Nat&) const;
  Nat operator<<(int) const;
  Nat operator++() const;
  Nat operator--() const;
  Nat operator+(const Nat&) const;
  Nat operator-(const Nat&) const;
  Nat operator*(const Nat&) const;
};