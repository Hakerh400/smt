#include "log.hh"
#include "games_info.hh"
#include "main.hh"

uint64_t seed = 1;
// uint64_t seed = 107901505544080;

void main1(){
  std::string name = read_str();
  std::vector<int> params = read_int_vec();
  // std::string name = "fillomino";
  // std::vector<int> params {5, 5, 9};

  Game* game = GamesInfo::mk(name, params);
  std::vector<int> out = game->generate();
  delete game;

  log(out.size());

  for(int x : out)
    log(x);
}

int main(){
  if(seed != 0){
    if(seed == 1){
      int* a = new int;
      seed = reinterpret_cast<uint64_t>(a);
      delete a;

      log(seed);
      log();
    }

    srand(seed);
  }

  main1();
  return 0;
}