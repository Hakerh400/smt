#pragma once

#include "util.hh"

template <typename T>
class Grid {
public:
  Grid(int, int, std::function<T(int, int)>);
  Grid(int w=0, int h=0);
  int get_width() const;
  int get_height() const;
  int get_size() const;
  std::vector<T>& get_data();
  bool has(int, int) const;
  T& get(int, int);
  T* getm(int, int);
  void set(int, int, const T&);
  std::vector<T> to_vector() const;
  void iter(std::function<void(int, int, T&)>);
  Grid<T> map(std::function<T(int, int, T&)>);

private:
  int w;
  int h;
  int size;
  std::vector<T> data;

  void mk(int, int, std::function<T(int, int)>);
};