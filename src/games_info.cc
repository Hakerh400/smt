#include "log.hh"
#include "games_info.hh"
#include "games/sudoku.hh"
#include "games/fillomino.hh"

const GamesInfoMpType& GamesInfo::get_mp(){
  return mp;
}

bool GamesInfo::has(std::string name){
  return mp.contains(name);
}

const GamesInfoFnType& GamesInfo::get(std::string name){
  return mp.at(name);
}

Game* GamesInfo::mk(std::string name, const std::vector<int>& params){
  return get(name)(params);
}

GamesInfoMpType GamesInfo::init_mp(){
  #define GamesInfo_entry(Ctor) \
    {Ctor::get_name(), [](const std::vector<int>& params){ \
      return (Game*)(new Ctor(params)); \
    }}
  
  return {
    GamesInfo_entry(Sudoku),
    GamesInfo_entry(Fillomino),
  };
  
  #undef GamesInfo_entry
}

GamesInfoMpType GamesInfo::mp = init_mp();