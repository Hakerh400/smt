#pragma once

#include "util.hh"
#include "nat.hh"

namespace CaDiCaL {
  class Solver;
};

class CSP {
public:
  static int bits_num(int);

  CSP();
  CSP(const CSP&) = delete;
  CSP(CSP&&) = delete;
  ~CSP();
  CSP& operator=(const CSP&) = delete;
  CSP& operator=(CSP&&) = delete;
  int get_vars_num() const;
  Bit mk_bit();
  std::vector<int> mk_lit_vec(int);
  BitVec mk_bit_vec(int);
  Nat nat_aux(int, int);
  Nat nat(int, int);
  Nat nat(int);
  Nat mk_nat_aux(int);
  Nat mk_nat(int);
  BitVec bit_vec_zero(int);
  void assume(int);
  void assume(const Bit&);
  bool solve();
  bool is_sat() const;
  bool get_lit(int);
  bool get_bit(const Bit&);
  int get_nat(const Nat&);
  void add_lit(int);
  void asrt_lit(int);
  void asrt(const Bit&);
  int lit(bool) const;
  const Bit& bit(bool) const;
  void add_clue_lits(const std::vector<int>&, int pri=0);
  void add_clue(const BitVec&, int pri=0);
  void add_clue(const Bit&, int pri=0);
  bool generate();
  void filter_clues(bool do_filter=1);
  bool has_clue(const Nat&);
  int get_clue(const Nat&);

private:
  CaDiCaL::Solver* solver;
  bool sat;
  int vars_num;
  Bit bit0;
  Bit bit1;
  Bit bit_g;
  int clues_num;
  int clue_bits_num;
  std::vector<std::vector<std::vector<int>>> clues;
  std::map<int, int> clues_res;
  // double percent;

  void set_percent(double);
};