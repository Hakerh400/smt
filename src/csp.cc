#include "log.hh"
#include "csp.hh"

#ifdef FLAG_LINUX
#include "/home/user/Git/other/cadical/src/cadical.hpp"
#endif
#ifdef FLAG_WINDOWS
#include "C:/Projects/Other/cadical/src/cadical.hpp"
#endif

int CSP::bits_num(int n){
  assert(n >= 0);

  int bits_num = 0;
  int k = 1;

  while(n >= k){
    bits_num++;
    k <<= 1;
  }

  return bits_num;
}
  
CSP::CSP(){
  solver = new CaDiCaL::Solver();
  sat = 0;

  vars_num = 0;
  clues_num = 0;
  clue_bits_num = 0;

  bit1 = mk_bit();
  bit0 = !bit1;
  bit_g = mk_bit();
  
  asrt(bit1);
}

CSP::~CSP(){
  delete solver;
}

int CSP::get_vars_num() const {
  return vars_num;
}

Bit CSP::mk_bit(){
  return Bit(this, ++vars_num);
}

std::vector<int> CSP::mk_lit_vec(int len){
  std::vector<int> lits = std::vector<int>(len);
  
  for(int i = 0; i != len; i++)
    lits[i] = ++vars_num;

  return lits;
}

BitVec CSP::mk_bit_vec(int len){
  return BitVec(this, mk_lit_vec(len));
}

Nat CSP::nat_aux(int len, int n){
  std::vector<int> lits = std::vector<int>(len);

  for(int i = 0; i != len; i++){
    lits[i] = lit(n & 1);
    n >>= 1;
  }

  assert(n == 0);

  return Nat(this, lits);
}

Nat CSP::nat(int mx, int n){
  return nat_aux(bits_num(mx), n);
}

Nat CSP::nat(int mx){
  return nat(mx, mx);
}

Nat CSP::mk_nat_aux(int len){
  std::vector<int> lits = mk_lit_vec(len);
  return Nat(this, lits);
}

Nat CSP::mk_nat(int mx){
  int len = bits_num(mx);
  Nat x = mk_nat_aux(len);
  asrt(x <= nat_aux(len, mx));
  return x;
}

BitVec CSP::bit_vec_zero(int len){
  std::vector<int> lits = mk_lit_vec(len);

  for(int i = 0; i != len; i++)
    lits[i] = -1;

  return BitVec(this, lits);
}

void CSP::assume(int lit){
  solver->assume(lit);
  // sat = 0;
}

void CSP::assume(const Bit& a){
  assume(a.get_lit());
}

bool CSP::solve(){
  int res = solver->solve();
  sat = res == 10;

  return sat;
}

bool CSP::is_sat() const {
  return sat;
}

bool CSP::get_lit(int lit){
  assert(sat);

  bool pos = lit > 0;
  int id = pos ? lit : -lit;
  int val = solver->val(id) > 0;
  
  return pos ? val : !val;
}

bool CSP::get_bit(const Bit& a){
  return get_lit(a.get_lit());
}

int CSP::get_nat(const Nat& a){
  int len = a.length();
  int n = 0;

  for(int i = len - 1; i >= 0; i--)
    n = (n << 1) | get_bit(a[i]);

  return n;
}

void CSP::add_lit(int lit){
  solver->add(lit);
  // sat = 0;
}

void CSP::asrt_lit(int lit){
  add_lit(lit);
  add_lit(0);
}

void CSP::asrt(const Bit& a){
  asrt_lit(a.get_lit());
}

int CSP::lit(bool b1) const {
  return b1 ? 1 : -1;
}

const Bit& CSP::bit(bool b1) const {
  return b1 ? bit1 : bit0;
}

void CSP::add_clue_lits(const std::vector<int>& xs, int pri){
  int len = xs.size();
  if(len == 0) return;

  while(clues.size() <= pri)
    clues.insert(clues.end(), std::vector<std::vector<int>>(0));

  std::vector<std::vector<int>>& list = clues.at(pri);
  list.insert(list.end(), xs);

  clues_num++;
  clue_bits_num += len;
}

void CSP::add_clue(const BitVec& xs, int pri){
  add_clue_lits(xs.get_lits(), pri);
}

void CSP::add_clue(const Bit& x, int pri){
  std::vector<int> lits({x.get_lit()});
  add_clue_lits(lits, pri);
}

bool CSP::generate(){
  if(!solve()) return 0;

  int bits_done = 0;

  for(std::vector<std::vector<int>> list : clues){
    int list_len = list.size();
    
    for(int i = 0; i != list_len; i++){
      // log("[1] ", 0);
      // log(i + 1, 0);
      // log(" / ", 0);
      // log(list_len);

      int index = i + rand() % (list_len - i);
      std::vector<int> xs = list.at(index);
      int xs_len = xs.size();

      list[index] = list.at(i);

      for(int j = 0; j != xs_len; j++){
        int index = j + rand() % (xs_len - j);
        int lit = xs.at(index);

        xs[index] = xs.at(j);

        set_percent((double)(bits_done++) / clue_bits_num / 2);

        bool bit = rand() & 1;

        if(get_lit(lit) == bit){
          asrt(bit_g.imp(Bit(this, bit ? lit : -lit)));
          assume(bit_g);
          assert(solve());
          continue;
        }

        if(bit) lit = -lit;
        
        assume(bit_g);
        assume(-lit);
        bool sat = solve();

        asrt(bit_g.imp(Bit(this, sat ? -lit : lit)));
        assume(bit_g);
        assert(solve());
      }
    }
  }

  return 1;
}

void CSP::filter_clues(bool do_filter){
  std::map<int, int>& res = clues_res;
  std::map<int, int> mp = {};
  Bit dif = bit(0);

  assert(solve());

  for(const std::vector<std::vector<int>>& list : clues){
    for(const std::vector<int>& xs : list){
      int len = xs.size();
      Nat x = Nat(this, xs);
      int key = xs.at(0);
      int n = get_nat(x);
      res[key] = n;
    }
  }

  for(const std::vector<std::vector<int>>& list : clues){
    for(const std::vector<int>& xs : list){
      int len = xs.size();
      Nat x = Nat(this, xs);
      int key = xs.at(0);
      int n = res.at(key);
      Bit b = x == nat_aux(len, n);
      dif = dif || !b;

      mp[key] = b.get_lit();
    }
  }

  int clues_done = 0;

  if(do_filter){
    for(std::vector<std::vector<int>> list : clues){
      int list_len = list.size();
      
      for(int i = 0; i != list_len; i++){
        // log("[2] ", 0);
        // log(i + 1, 0);
        // log(" / ", 0);
        // log(list_len);

        int index = i + rand() % (list_len - i);
        std::vector<int> xs = list.at(index);

        list[index] = list.at(i);

        int key = xs.at(0);

        // log();
        // log("key ", 0);
        // log(key);

        set_percent(0.5 + (double)(clues_done++) / clues_num / 2);

        assume(dif);

        // log("#", 0);
        for(const std::pair<int, int>& kv : mp){
          int key1 = kv.first;
          int val = kv.second;

          if(key1 == key) continue;
          // log(" ", 0);
          // log(key1, 0);
          assume(val);
        }
        // log();

        bool sat = solve();
        // log("sat ", 0);
        // log(sat);

        if(sat) continue;

        assert(mp.contains(key));
        assert(res.contains(key));

        mp.erase(key);
        res.erase(key);
      }
    }
  }

  /*assume(dif);
  for(const std::pair<int, int>& kv : mp){
    int key = kv.first;
    int val = kv.second;

    // log(key, 0);
    // log(" ", 0);
    // log(val);

    assume(val);
  }
  assert(!solve());*/

  // Clear assumptions
  assume(bit(0));
  assert(!solve());

  assert(solve());
}

bool CSP::has_clue(const Nat& x){
  if(x.null()) return 0;

  int key = x[0].get_lit();
  return clues_res.contains(key);
}

int CSP::get_clue(const Nat& x){
  assert(!x.null());

  int key = x[0].get_lit();
  return clues_res.at(key);
}

void CSP::set_percent(double p){
  // int percent1 = floor(p * 100 + 0.5);

  // assert(percent1 >= percent);
  // assert(percent1 <= 100);

  // if(percent1 <= percent)
  //   return;

  // percent = percent1;

  log("(", 0);
  log(p, 0);
  log(")");
}