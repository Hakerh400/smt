#include "csp.hh"
#include "nat.hh"

Bit Nat::le_or_lt(bool is_le, const Nat& b) const {
  const Nat& a = *this;
  int len = length();
  assert(b.length() == len);

  Bit x = csp->bit(is_le);

  for(int i = 0; i != len; i++){
    Bit b1 = a[i];
    Bit b2 = b[i];
    x = (!b1 && b2) || (b1 == b2 && x);
  }

  return x;
}

Nat Nat::inc(const Bit& c0) const {
  const Nat& a = *this;
  int len = length();

  Bit c = c0;
  std::vector<int> xs = std::vector<int>(len);

  for(int i = 0; i != len; i++){
    Bit b1 = a[i];
    xs[i] = (b1 ^ c).get_lit();
    c = b1 && c;
  }

  csp->asrt(!c);

  return Nat(csp, xs);
}

Nat Nat::inc() const {
  return inc(csp->bit(1));
}

Bit Nat::operator<=(const Nat& b) const {
  return le_or_lt(1, b);
}

Bit Nat::operator>=(const Nat& b) const {
  return b <= *this;
}

Bit Nat::operator<(const Nat& b) const {
  return le_or_lt(0, b);
}

Bit Nat::operator>(const Nat& b) const {
  return b < *this;
}

Nat Nat::operator<<(int k) const {
  int len = length();
  std::vector<int> xs = std::vector<int>(len);
  
  for(int i = 0; i != len; i++)
    xs[i] = i < k ? csp->lit(0) : lits[i - k];
  
  for(int i = 0; i != k; i++)
    csp->asrt_lit(-lits[len - i - 1]);
  
  return Nat(csp, xs);
}

Nat Nat::operator++() const {
  return inc();
}

Nat Nat::operator--() const {
  const Nat& a = *this;
  int len = length();

  Bit c = csp->bit(1);
  std::vector<int> xs = std::vector<int>(len);

  for(int i = 0; i != len; i++){
    Bit b1 = a[i];
    xs[i] = (b1 ^ c).get_lit();
    c = !b1 && c;
  }

  csp->asrt(!c);

  return Nat(csp, xs);
}

Nat Nat::operator+(const Nat& b) const {
  const Nat& a = *this;
  int len = length();
  assert(b.length() == len);

  Bit c = csp->bit(0);
  std::vector<int> xs = std::vector<int>(len);

  for(int i = 0; i != len; i++){
    Bit b1 = a[i];
    Bit b2 = b[i];
    xs[i] = (b1 ^ b2 ^ c).get_lit();
    c = c.ite(b1 || b2, b1 && b2);
  }

  csp->asrt(!c);

  return Nat(csp, xs);
}

Nat Nat::operator-(const Nat& b) const {
  const Nat& a = *this;
  int len = length();
  assert(b.length() == len);

  Bit c = csp->bit(0);
  std::vector<int> xs = std::vector<int>(len);

  for(int i = 0; i != len; i++){
    Bit b1 = a[i];
    Bit b2 = b[i];
    xs[i] = (b1 ^ b2 ^ c).get_lit();
    c = c.ite(!b1 || b2, !b1 && b2);
  }

  csp->asrt(!c);

  return Nat(csp, xs);
}

Nat Nat::operator*(const Nat& b) const {
  const Nat& a = *this;
  int len = length();
  assert(b.length() == len);
  
  Nat sum = csp->nat_aux(len, 0);
  
  for(int i = 0; i != len; i++){
    Bit b1 = b[i];
    std::vector<int> xs = std::vector<int>(len);
    
    for(int j = 0; j != len; j++)
      xs[j] = (b1 && a[j]).get_lit();
    
    sum = sum + (Nat(csp, xs) << i);
  }
  
  return sum;
}

Bit Nat::is_zero() const {
  return all_zeros();
}

Bit Nat::nonzero() const {
  return !is_zero();
}