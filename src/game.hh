#pragma once

#include "csp.hh"

class Game {
public:
  static std::string get_name();

  Game(const std::vector<int>&);
  Game(const Game&) = delete;
  Game(Game&&) = delete;
  ~Game();
  Game& operator=(const Game&) = delete;
  Game& operator=(Game&&) = delete;
  CSP* get_csp();
  const std::vector<int>& get_params() const;
  virtual std::vector<int> generate(){
    assert(0);
    return {};
  }

private:
  CSP* csp;
  std::vector<int> params;
};