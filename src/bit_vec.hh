#pragma once

#include "bit.hh"

class BitVec {
public:
  BitVec() = default;
  BitVec(CSP*, const std::vector<int>&);
  CSP* get_csp() const;
  const std::vector<int>& get_lits() const;
  int length() const;
  bool null() const;
  Bit at_least_one() const;
  Bit at_most_one() const;
  Bit exactly_one() const;
  Bit all_zeros() const;
  Bit all_ones() const;
  Bit operator[](int) const;
  Bit operator==(const BitVec&) const;
  Bit operator!=(const BitVec&) const;
  BitVec operator|(const BitVec&) const;
  BitVec operator&(const BitVec&) const;

protected:
  CSP* csp;
  std::vector<int> lits;
};