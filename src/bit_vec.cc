#include "log.hh"
#include "csp.hh"
#include "bit_vec.hh"

BitVec::BitVec(CSP* csp, const std::vector<int>& lits){
  this->csp = csp;
  this->lits = lits;
}

CSP* BitVec::get_csp() const {
  return csp;
}

const std::vector<int>& BitVec::get_lits() const {
  return lits;
}

int BitVec::length() const {
  return lits.size();
}

bool BitVec::null() const {
  return length() == 0;
}

Bit BitVec::at_least_one() const {
  Bit b = csp->bit(0);

  for(int lit : lits)
    b = b || Bit(csp, lit);

  return b;
}

Bit BitVec::at_most_one() const {
  int len = length();
  int len1 = len - 1;

  std::vector<int> xs = std::vector<int>(len);
  std::vector<int> ys = std::vector<int>(len);

  xs[0] = csp->lit(1);
  ys[len - 1] = csp->lit(1);

  for(int i = 1; i < len; i++){
    Bit b1 = Bit(csp, -lits[i - 1]);
    Bit b2 = Bit(csp, -lits[len - i]);

    xs[i] = (Bit(csp, xs[i - 1]) && b1).get_lit();
    ys[len1 - i] = (Bit(csp, ys[len - i]) && b2).get_lit();
  }

  Bit b = csp->bit(0);

  for(int i = 0; i != len; i++)
    b = b || (Bit(csp, xs[i]) && Bit(csp, ys[i]));

  return b;
}

Bit BitVec::exactly_one() const {
  return at_least_one() && at_most_one();
}

Bit BitVec::all_zeros() const {
  Bit b = csp->bit(1);

  for(int lit : lits)
    b = b && Bit(csp, -lit);

  return b;
}

Bit BitVec::all_ones() const {
  Bit b = csp->bit(1);

  for(int lit : lits)
    b = b && Bit(csp, lit);

  return b;
}

Bit BitVec::operator[](int i) const {
  return Bit(csp, lits.at(i));
}

Bit BitVec::operator==(const BitVec& b) const {
  const BitVec& a = *this;
  int len = length();
  assert(b.length() == len);

  Bit x = csp->bit(1);

  for(int i = 0; i != len; i++)
    x = x && a[i] == b[i];

  return x;
}

Bit BitVec::operator!=(const BitVec& b) const {
  return !(*this == b);
}

BitVec BitVec::operator|(const BitVec& b) const {
  const BitVec& a = *this;
  int len = length();
  assert(b.length() == len);

  std::vector<int> lits = std::vector<int>(len);

  for(int i = 0; i != len; i++)
    lits[i] = (a[i] || b[i]).get_lit();

  return BitVec(csp, lits);
}

BitVec BitVec::operator&(const BitVec& b) const {
  const BitVec& a = *this;
  int len = length();
  assert(b.length() == len);

  std::vector<int> lits = std::vector<int>(len);

  for(int i = 0; i != len; i++)
    lits[i] = (a[i] && b[i]).get_lit();

  return BitVec(csp, lits);
}