#include "log.hh"
#include "game.hh"

Game::Game(const std::vector<int>& params){
  csp = new CSP();
  this->params = params;
}

Game::~Game(){
  delete csp;
}

CSP* Game::get_csp(){
  return csp;
}

const std::vector<int>& Game::get_params() const {
  return params;
}